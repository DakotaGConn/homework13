import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 * Created by Tim McGowen on 3/22/2017
 *
 * This is the controller for the FXML document that contains the view. 
 */
public class WxController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtzipcode;

  @FXML
  private Label lblcity;

  @FXML
  private Label lbltemp;
  
  @FXML
  private Label lbltime;
  
  @FXML
  private Label lblweather;
  
  @FXML
  private Label lblwind;
  
  @FXML
  private Label lblpressure;
  
  @FXML
  private Label lblvisibility;

  @FXML
  private ImageView iconwx;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    WxModel weather = new WxModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
      String zipcode = txtzipcode.getText();
      if (weather.getWx(zipcode))
      {
        lblcity.setText(weather.getLocation());
        lbltemp.setText(String.valueOf(weather.getTemp()));
        lbltime.setText(String.valueOf(weather.getTime()));
        lblweather.setText(String.valueOf(weather.getWeather()));
        lblwind.setText(String.valueOf(weather.getWind()));
        lblpressure.setText(String.valueOf(weather.getPressure()));
        lblvisibility.setText(String.valueOf(weather.getVisibility()));
        iconwx.setImage(weather.getImage());
      }
      else
      {
        lblcity.setText("Invalid Zipcode");
        lbltemp.setText("");
        lbltime.setText("");
        lblweather.setText("");
        lblwind.setText("");
        lblpressure.setText("");
        lblvisibility.setText("");
        iconwx.setImage(new Image("badzipcode.png"));
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
